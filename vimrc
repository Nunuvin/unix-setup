set number relativenumber
set clipboard=unnamed
filetype plugin on
syntax enable
set tabstop=4 softtabstop=0 expandtab shiftwidth=4 smarttab
